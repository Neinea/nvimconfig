local opt = vim.opt

opt.tabstop = 2
opt.softtabstop = 2
opt.shiftwidth = 2
opt.textwidth = 120
opt.expandtab = true
opt.autoindent = true
opt.fileformat = "unix"
