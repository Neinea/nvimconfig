local nvimtree = require("nvim-tree")
local vimp = require("vimp")

nvimtree.setup({
  open_on_setup = true
})

vimp.nnoremap("<leader><Space>", function()
  if require("nvim-tree.view").is_visible() then
    vim.cmd("NvimTreeClose")
  else
    vim.cmd("NvimTreeOpen")
  end
end)
