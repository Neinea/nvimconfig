local map = vim.api.nvim_set_keymap

local silent = { silent = true, noremap = true }
-- Navigate buffers and repos
map('n', '<leader><leader>p', [[<cmd>Telescope buffers show_all_buffers=true theme=get_dropdown<cr>]], silent)
map('n', '<leader><leader>P', [[<cmd>Telescope commands theme=get_dropdown<cr>]], silent)
map('n', '<leader><leader>a', [[<cmd>Telescope buffers show_all_buffers=true theme=get_dropdown<cr>]], silent)
map('n', '<leader><leader>e', [[<cmd>Telescope frecency theme=get_dropdown<cr>]], silent)
map('n', '<leader><leader>s', [[<cmd>Telescope git_files theme=get_dropdown<cr>]], silent)
map('n', '<leader><leader>d', [[<cmd>Telescope find_files theme=get_dropdown<cr>]], silent)
map('n', '<leader><leader>g', [[<cmd>Telescope live_grep theme=get_dropdown<cr>]], silent)
