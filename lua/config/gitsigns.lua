require('gitsigns').setup {
  signs = {
    add = { hl = 'GreenSign', text = '│', numhl = 'GitSignsAddNr' },
    change = { hl = 'BlueSign', text = '│', numhl = 'GitSignsChangeNr' },
    delete = { hl = 'RedSign', text = '│', numhl = 'GitSignsDeleteNr' },
    topdelete = { hl = 'RedSign', text = '│', numhl = 'GitSignsDeleteNr' },
    changedelete = { hl = 'PurpleSign', text = '│', numhl = 'GitSignsChangeNr' },
  },
  current_line_blame = true,
  current_line_blame_opts = {
    virt_text = true,
    virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
    delay = 1000,
    ignore_whitespace = false,
  },
  keymaps = {},
}
