vim.cmd [[packadd packer.nvim]]

local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'
	use 'tpope/vim-surround'
	use 'tpope/vim-commentary'
	use 'tpope/vim-repeat'
	use {
    'neovim/nvim-lspconfig',
    config = [[require('config.lspconfig')]]
  }
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/cmp-cmdline'
	use {
    'hrsh7th/nvim-cmp',
    config = [[require('config.cmp')]]
  }
	use 'L3MON4D3/LuaSnip'
	use 'saadparwaiz1/cmp_luasnip'
	use {
		'nvim-telescope/telescope.nvim',
		tag = '0.1.0',
		requires = {
      'nvim-lua/plenary.nvim',
      'svermeulen/vimpeccable',
    },
    config = [[require('config.telescope')]]
	}
	use {
		"windwp/nvim-autopairs",
    config = function()
      require("nvim-autopairs").setup {}
    end
	}
	use {'dracula/vim', as = 'dracula'}
	use 'folke/neodev.nvim'
	use {
	  'nvim-tree/nvim-tree.lua',
	  requires = {
	    'nvim-tree/nvim-web-devicons',
      'svermeulen/vimpeccable'
	  },
    config = [[require('config.nvimtree')]]
	}
  use {
    'akinsho/bufferline.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
    config = [[require('config.bufferline')]]
  }
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
    config = [[require('config.lualine')]]
  }
  use {
    'lewis6991/gitsigns.nvim',
    config = [[require('config.gitsigns')]]
  }
 	use {
    'TimUntersberger/neogit',
    requires = 'nvim-lua/plenary.nvim'
  }
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
  }
  use 'sindrets/diffview.nvim'
  use {
    'weilbith/nvim-code-action-menu',
    cmd = 'CodeActionMenu'
  }
  use {
    'simrat39/rust-tools.nvim',
    requires = {
      'neovim/nvim-lspconfig',
    },
    config = [[require('config.rusttools')]]
  }
	if packer_bootstrap then
    require('packer').sync()
	end
end)
