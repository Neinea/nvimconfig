require('plugins')
local cmd = vim.cmd

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.termguicolors = true
vim.opt.nu = true
vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
vim.opt.autowrite = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.g.mapleader = ","
cmd.colorscheme('dracula')

vim.api.nvim_set_keymap("n", "<leader>1", "1gt", {})
vim.api.nvim_set_keymap("n", "<leader>2", "2gt", {})
vim.api.nvim_set_keymap("n", "<leader>3", "3gt", {})
vim.api.nvim_set_keymap("n", "<leader>4", "4gt", {})
vim.api.nvim_set_keymap("n", "<leader>5", "5gt", {})
vim.api.nvim_set_keymap("n", "<leader>6", "6gt", {})
vim.api.nvim_set_keymap("n", "<C-Down>", "<C-W><Down>", {})
vim.api.nvim_set_keymap("n", "<C-Up>", "<C-W><Up>", {})
vim.api.nvim_set_keymap("n", "<C-Right>", "<C-W><Right>", {})
vim.api.nvim_set_keymap("n", "<C-Left>", "<C-W><Left>", {})
vim.api.nvim_set_keymap("v", "<C-c>", "+yy", {noremap = true})
vim.api.nvim_set_keymap("n", "<leader>b", ":bprev<CR>", {})
vim.api.nvim_set_keymap("n", "<leader>n", ":bnext<CR>", {})
vim.api.nvim_set_keymap("t", "<Esc>", '<C-\\><C-n>', {noremap = true})
vim.api.nvim_set_keymap("n", "<leader>t", "<Cmd>10sp|term<CR>", {})
